use serde::Deserialize;
use std::collections::HashMap;

use secrecy::Secret;

#[derive(Deserialize, Debug)]
pub struct KeyValueReadResponse {
    pub data: HashMap<String, Secret<String>>,
}

#[derive(Deserialize, Debug)]
pub struct ErrorResponse {
    pub errors: Vec<String>,
}
