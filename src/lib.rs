#![forbid(unsafe_code)]

mod concurrency_util;
mod models;
mod serde_util;

use async_trait::async_trait;

use secrecy::ExposeSecret;
use secrecy::Secret;
use xand_secrets::{CheckHealthError, ReadSecretError, SecretKeyValueStore};

use reqwest::{Certificate, Client as HttpClient, Response as HttpResponse, StatusCode};

use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::Read;

use concurrency_util::concurrent_and;
use thiserror::Error;

const TOKEN_HEADER: &str = "X-Vault-Token";

#[derive(Debug, Error)]
pub enum VaultSecretStoreCreationError {
    #[error("creating the internal client failed. {internal_error}")]
    ClientCreation {
        #[source]
        internal_error: reqwest::Error,
    },
    #[error("could not parse the given certificate file. {internal_error}")]
    CertificateFormat {
        #[source]
        internal_error: reqwest::Error,
    },
    #[error("failed to open the provided certificate pem file. {internal_error}")]
    CertificateFileLoad {
        #[source]
        internal_error: std::io::Error,
    },
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
#[serde(deny_unknown_fields)]
pub struct VaultConfiguration {
    pub http_endpoint: String,
    #[serde(serialize_with = "serde_util::serialize_secret")]
    pub token: Secret<String>,
    pub additional_https_root_certificate_files: Option<Vec<String>>,
}

#[derive(Debug)]
pub struct VaultSecretKeyValueStore {
    config: VaultConfiguration,
    client: reqwest::Client,
}

#[derive(Debug)]
pub struct VaultKey {
    pub path: String,
    pub key: String,
}

impl VaultKey {
    fn parse_from_slash_separated_key(key: &str) -> Result<Self, ReadSecretError> {
        key.rfind('/')
            .map(|index| VaultKey {
                path: String::from(&key[..index]),
                key: String::from(&key[index + 1..]),
            })
            .ok_or(ReadSecretError::KeyNotFound {
                key: key.to_string(),
            })
    }
}

impl VaultSecretKeyValueStore {
    pub fn create_from_config(
        config: VaultConfiguration,
    ) -> Result<VaultSecretKeyValueStore, VaultSecretStoreCreationError> {
        let client = Self::build_client(&config)?;
        Ok(VaultSecretKeyValueStore { config, client })
    }

    fn build_client(
        config: &VaultConfiguration,
    ) -> Result<HttpClient, VaultSecretStoreCreationError> {
        let mut builder = HttpClient::builder();

        for cert_path in config
            .additional_https_root_certificate_files
            .iter()
            .flatten()
        {
            builder = builder.add_root_certificate(Self::load_certificate_pem(cert_path.as_str())?);
        }

        builder.build().map_err(
            |internal_error| VaultSecretStoreCreationError::ClientCreation { internal_error },
        )
    }

    fn load_certificate_pem(cert_path: &str) -> Result<Certificate, VaultSecretStoreCreationError> {
        let mut buf = Vec::new();
        File::open(cert_path)
            .and_then(|mut f| f.read_to_end(&mut buf))
            .map_err(
                |internal_error| VaultSecretStoreCreationError::CertificateFileLoad {
                    internal_error,
                },
            )?;

        reqwest::Certificate::from_pem(&buf).map_err(|internal_error| {
            VaultSecretStoreCreationError::CertificateFormat { internal_error }
        })
    }

    fn remove_trailing_slash(path: &str) -> &str {
        let len = path.len();
        if path.ends_with('/') {
            &path[..len - 1]
        } else {
            path
        }
    }

    fn get_url_for_path(&self, path: &str) -> String {
        format!(
            "{}/v1/{}",
            Self::remove_trailing_slash(&self.config.http_endpoint[..]),
            path
        )
    }

    fn map_reqwest_error_to_read_error(error: reqwest::Error) -> ReadSecretError {
        ReadSecretError::Request {
            internal_error: Box::new(error),
        }
    }

    async fn parse_read_response(
        response: HttpResponse,
    ) -> Result<models::KeyValueReadResponse, ReadSecretError> {
        response
            .json::<models::KeyValueReadResponse>()
            .await
            .map_err(Self::map_reqwest_error_to_read_error)
    }

    async fn get_error_list(response: HttpResponse) -> Option<Vec<String>> {
        response
            .json::<models::ErrorResponse>()
            .await
            .map(|error_response| error_response.errors)
            .ok()
    }

    async fn map_status_code_to_read_error(
        response: HttpResponse,
        full_key: &str,
    ) -> ReadSecretError {
        let status_code = response.status();

        if let StatusCode::NOT_FOUND = status_code {
            return ReadSecretError::KeyNotFound {
                key: full_key.to_owned(),
            };
        }

        let internal_error = Box::new(VaultHttpError::new(
            response.url().path().to_owned(),
            status_code,
            Self::get_error_list(response).await,
        ));
        match status_code {
            StatusCode::FORBIDDEN => ReadSecretError::Authentication { internal_error },
            _ => ReadSecretError::Request { internal_error },
        }
    }

    async fn probe_health_at_path(&self, path: &str) -> Result<HttpResponse, reqwest::Error> {
        let http_url = self.get_url_for_path(path);

        let health_response = self
            .client
            // We use GET rather than HEAD because most Vault endpoints don't support HEAD requests.
            .get(&http_url[..])
            .header(TOKEN_HEADER, self.config.token.expose_secret())
            .send()
            .await?;

        Ok(health_response)
    }

    async fn map_status_code_to_check_health_result(
        response: HttpResponse,
    ) -> Result<(), CheckHealthError> {
        match response.status() {
            StatusCode::OK => Ok(()),
            StatusCode::FORBIDDEN => Err(CheckHealthError::Authentication {
                internal_error: Box::new(VaultHttpError::new(
                    String::from(response.url().path()),
                    response.status(),
                    None,
                )),
            }),
            _ => Err(CheckHealthError::RemoteInternal {
                internal_error: Box::new(VaultHttpError::new(
                    String::from(response.url().path()),
                    response.status(),
                    None,
                )),
            }),
        }
    }

    async fn check_vault_system_health(&self) -> Result<(), CheckHealthError> {
        // The health endpoint returns OK status iff the vault is initialized
        // and unsealed -- i.e., useable. However, it does NOT check
        // authentication. This endpoint is public by default.

        // Status codes documented at: https://www.vaultproject.io/api-docs/system/health

        let health_path = "sys/health";
        match self.probe_health_at_path(health_path).await {
            // 429 indicates "unsealed and standby"; in this mode, the Vault is able to handle read
            // requests, but is only serving as a proxy to a dedicated master. We consider this "healthy".
            Ok(response) if response.status().as_u16() == 429 => Ok(()),
            // 473 indicates "performance standby". Similar to 429, the Vault is forwarding requests
            // to a separate Vault; this code differs from above only in that the cluster is
            // configured in "high-availability" mode rather than with a user-chosen master. We
            // consider this "healthy".
            Ok(response) if response.status().as_u16() == 473 => Ok(()),
            Ok(response) => Self::map_status_code_to_check_health_result(response).await,
            Err(e) => Err(CheckHealthError::Unreachable {
                internal_error: Box::new(e),
            }),
        }
    }

    async fn check_vault_credential_health(&self) -> Result<(), CheckHealthError> {
        // We use the auth API to validate our credentials. This endpoint is
        // chosen because it requires authentication, but the actual content
        // of the response is entirely ignored.
        let auth_path = "sys/auth";
        match self.probe_health_at_path(auth_path).await {
            Ok(response) => Self::map_status_code_to_check_health_result(response).await,
            Err(e) => Err(CheckHealthError::Unreachable {
                internal_error: Box::new(e),
            }),
        }
    }
}

#[async_trait]
impl SecretKeyValueStore for VaultSecretKeyValueStore {
    async fn read(&self, key: &str) -> Result<Secret<String>, ReadSecretError> {
        let VaultKey {
            path: split_path,
            key: split_key,
        } = VaultKey::parse_from_slash_separated_key(key)?;

        let http_url = self.get_url_for_path(split_path.as_str());

        let response = self
            .client
            .get(&http_url[..])
            .header(TOKEN_HEADER, self.config.token.expose_secret())
            .send()
            .await
            .map_err(Self::map_reqwest_error_to_read_error)?;

        if !response.status().is_success() {
            return Err(Self::map_status_code_to_read_error(response, key).await);
        }

        let response_body = VaultSecretKeyValueStore::parse_read_response(response).await?;

        let secret =
            response_body
                .data
                .get(split_key.as_str())
                .ok_or(ReadSecretError::KeyNotFound {
                    key: key.to_owned(),
                })?;

        Ok(secret.to_owned())
    }

    async fn check_health(&self) -> Result<(), CheckHealthError> {
        concurrent_and(
            self.check_vault_system_health(),
            self.check_vault_credential_health(),
        )
        .await
    }
}

#[derive(Debug)]
struct VaultHttpError {
    pub path: String,
    pub status_code: StatusCode,
    pub errors: Option<Vec<String>>,
}

impl VaultHttpError {
    pub fn new(
        path: String,
        status_code: StatusCode,
        errors: Option<Vec<String>>,
    ) -> VaultHttpError {
        VaultHttpError {
            path,
            status_code,
            errors,
        }
    }
}

impl std::error::Error for VaultHttpError {}
impl std::fmt::Display for VaultHttpError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
        write!(
            f,
            "An error code was returned while making an HTTP request to path \"{}\". Status code: {}.{}",
            self.path,
            self.status_code.as_str(),
            self.errors.as_ref().map_or(String::default(), |e| format!(" Vault returned errors: {:?}", e))
        )
    }
}
