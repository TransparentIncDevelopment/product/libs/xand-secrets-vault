use futures::future::Either::{Left, Right};
use std::future::Future;

/// Like Result<T, E>::and, but operates on two Futures returning Result.
///
/// As with "and", if a evaluates to an Err, that value is returned; otherwise,
/// b is evaluated and its output is returned.
///
/// Unlike "and", the two futures are evaluated concurrently; if "a" errors, we
/// don't need to wait for b. If "a" succeeds, we've already gotten a head-start
/// on evaluation of b.
pub async fn concurrent_and<T, U, E, A, B>(a: A, b: B) -> Result<U, E>
where
    A: Future<Output = Result<T, E>>,
    B: Future<Output = Result<U, E>>,
{
    futures::pin_mut!(a);
    futures::pin_mut!(b);
    match futures::future::select(a, b).await {
        Left((Ok(_), b)) => b.await,
        Left((Err(e), _)) => Err(e),
        Right((b_result, a)) => a.await.and(b_result),
    }
}

#[cfg(test)]
mod tests {
    use super::concurrent_and;
    use futures::{future::ready, Future};
    use std::time::Duration;

    const LONG_DELAY: Duration = Duration::from_millis(250);

    #[derive(Debug, Eq, PartialEq)]
    struct A;
    #[derive(Debug, Eq, PartialEq)]
    struct B;

    #[derive(Debug, Eq, PartialEq)]
    enum Errors {
        A,
        B,
    }

    async fn pending<T>() -> T {
        futures::pending!();
        unreachable!();
    }

    async fn delayed_resolve<T>(val: T) -> T {
        tokio::time::sleep(LONG_DELAY).await;
        val
    }

    fn a_resolves_first(
        a: Result<A, Errors>,
        b: Result<B, Errors>,
    ) -> (
        impl Future<Output = Result<A, Errors>>,
        impl Future<Output = Result<B, Errors>>,
    ) {
        (ready(a), delayed_resolve(b))
    }

    fn b_resolves_first(
        a: Result<A, Errors>,
        b: Result<B, Errors>,
    ) -> (
        impl Future<Output = Result<A, Errors>>,
        impl Future<Output = Result<B, Errors>>,
    ) {
        (delayed_resolve(a), ready(b))
    }

    // Annotated test cases index:
    //    A finishes first
    //        A errors -> returns A's error even if B never resolves
    //        A passes
    //            B passes -> returns B's value
    //            B errors -> returns B's error
    //    B finishes first
    //        B errors
    //            A errors -> A's error
    //            A passes -> B's error
    //        B passes
    //            A errors -> A's error
    //            A passes -> B's value

    #[tokio::test]
    async fn a_errors_first_b_never_resolves() {
        let a = ready::<Result<(), Errors>>(Err(Errors::A));
        let b = pending::<Result<(), _>>();

        let result = concurrent_and(a, b).await;
        assert_eq!(result, Err(Errors::A));
    }

    #[tokio::test]
    async fn a_passes_first_b_passes() {
        let (a, b) = a_resolves_first(Ok(A), Ok(B));

        let result = concurrent_and(a, b).await;
        assert_eq!(result, Ok(B));
    }

    #[tokio::test]
    async fn a_passes_first_b_errors() {
        let (a, b) = a_resolves_first(Ok(A), Err(Errors::B));

        let result = concurrent_and(a, b).await;
        assert_eq!(result, Err(Errors::B));
    }

    #[tokio::test]
    async fn b_errors_first_a_errors() {
        let (a, b) = b_resolves_first(Err(Errors::A), Err(Errors::B));

        let result = concurrent_and(a, b).await;
        assert_eq!(result, Err(Errors::A));
    }

    #[tokio::test]
    async fn b_errors_first_a_passes() {
        let (a, b) = b_resolves_first(Ok(A), Err(Errors::B));

        let result = concurrent_and(a, b).await;
        assert_eq!(result, Err(Errors::B));
    }

    #[tokio::test]
    async fn b_passes_first_a_errors() {
        let (a, b) = b_resolves_first(Err(Errors::A), Ok(B));

        let result = concurrent_and(a, b).await;
        assert_eq!(result, Err(Errors::A));
    }

    #[tokio::test]
    async fn b_passes_first_a_passes() {
        let (a, b) = b_resolves_first(Ok(A), Ok(B));

        let result = concurrent_and(a, b).await;
        assert_eq!(result, Ok(B));
    }
}
