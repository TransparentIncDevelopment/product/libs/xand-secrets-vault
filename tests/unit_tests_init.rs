#![forbid(unsafe_code)]
use matches::assert_matches;
use secrecy::Secret;
use std::io::Write;
use tempfile::NamedTempFile;
use xand_secrets_vault::{
    VaultConfiguration, VaultSecretKeyValueStore, VaultSecretStoreCreationError,
};

const TEST_TOKEN: &str = "token123";

#[test]
fn test_load_valid_certificate() {
    let kv_store = VaultSecretKeyValueStore::create_from_config(VaultConfiguration {
        http_endpoint: mockito::server_url(),
        token: Secret::new(String::from(TEST_TOKEN)),
        additional_https_root_certificate_files: Some(vec![String::from(
            // This certificate was generated specifically for these tests, and the
            // private key was discarded.
            "tests/certs/nonfunctional_dummy_root_ca.pem",
        )]),
    });

    assert_matches!(kv_store, Ok(_));
}

#[test]
fn test_load_certificate_error() {
    let kv_store = VaultSecretKeyValueStore::create_from_config(VaultConfiguration {
        http_endpoint: mockito::server_url(),
        token: Secret::new(String::from(TEST_TOKEN)),
        additional_https_root_certificate_files: Some(vec![String::from("some/invalid/file/path")]),
    });

    assert_matches!(
        kv_store,
        Err(VaultSecretStoreCreationError::CertificateFileLoad { internal_error: _ })
    );
}

#[test]
fn test_certificate_format_error() {
    let mut test_cert_file = NamedTempFile::new().unwrap();
    test_cert_file
        .write_all(b"This is not a valid certificate")
        .unwrap();
    let kv_store = VaultSecretKeyValueStore::create_from_config(VaultConfiguration {
        http_endpoint: mockito::server_url(),
        token: Secret::new(String::from(TEST_TOKEN)),
        additional_https_root_certificate_files: Some(vec![String::from(
            test_cert_file.path().to_str().unwrap(),
        )]),
    });

    assert_matches!(
        kv_store,
        Err(VaultSecretStoreCreationError::CertificateFormat { internal_error: _ })
    );
}
