# Test certificates

All certificates in this folder are used only for tests, and should not be used
for any other purpose.

`nonfunctional_dummy_root_ca.pem` is a root CA certificate whose private key has
been discarded. It's used only by the unit tests to confirm that a valid
certificate can be parsed and loaded.

`rootCA.pem` is a self-signed root CA certificate and `rootCA.key` is its
accompanying key.
`vault.pem` and `vault.key` are a derived certificate/key pair which the Vault
instance presents to clients in the integration test environment.
`valid_test_cert_chain/rebuild_certs.sh` was used to generate these
certificates and keys. They should already be available and ready for use
in source control, but this script can be run to re-generate them if needed.
