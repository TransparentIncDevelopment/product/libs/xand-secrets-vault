#! /bin/bash

# Adapted from: https://gist.github.com/fntlnz/cf14feb5a46b2eda428e000157447309

# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

CONFIG="
[ req ]
distinguished_name	= req_distinguished_name
attributes		= req_attributes
emailAddress        	= myEmail@email.com
req_extensions          = v3_req
x509_extensions         = v3_ca

[ req_distinguished_name ]
countryName                     = Country Name (2 letter code)
countryName_default             = AU
countryName_min                 = 2
countryName_max                 = 2
stateOrProvinceName             = State or Province Name (full name)
stateOrProvinceName_default     = Some-State
localityName                    = Locality Name (eg, city)
0.organizationName              = Organization Name (eg, company)
0.organizationName_default      = Internet Widgits Pty Ltd
organizationalUnitName          = Organizational Unit Name (eg, section)
commonName                      = Common Name (e.g. server FQDN or YOUR name)
commonName_max                  = 64
emailAddress                    = Email Address
emailAddress_max                = 64

[ req_attributes ]
challengePassword		= A challenge password
challengePassword_min		= 4
challengePassword_max		= 20

[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment

[ v3_ca ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
basicConstraints = critical,CA:true
"

# ROOT CA SETUP ==============
# Create a root private key and root CA cert
openssl genrsa -out "$DIR/rootCA.key" 4096
openssl req -x509 -config <(echo "$CONFIG") -extensions v3_ca -new -subj "/C=US/O=xand-secrets-vault integration tests fake root CA" -nodes -key "$DIR/rootCA.key" -sha256 -days 5000 -out "$DIR/rootCA.pem"


# VAULT ======================
# Generate a private key for the Vault instance
openssl genrsa -out "$DIR/vault.key" 2048
# Create a certificate request for the Vault
openssl req -new -sha256 -key "$DIR/vault.key" -subj "/C=US/ST=WA/O=xand-secrets-vault integration tests fake Vault/CN=localhost" -out "$DIR/vault.csr"
# Generate a certificate for the Vault
openssl x509 -req -in "$DIR/vault.csr" -CA "$DIR/rootCA.pem" -CAkey "$DIR/rootCA.key" -CAcreateserial -out "$DIR/vault.pem" -days 5000 -sha256

# Ensure the docker container is allowed to read the Vault cert and private key
chmod 666 "$DIR/vault.pem"
chmod 666 "$DIR/vault.key"
